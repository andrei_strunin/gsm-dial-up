﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularCoder
{
    class NRZEncoder
    {
        public byte[] Encode(byte[] data)
        {
            var tb = BitConverter.GetBytes(short.MinValue);

            MemoryStream ms = new MemoryStream();
            BitArray bits = new BitArray(data);


            for (int i = 0; i < bits.Length; i++) {
                if (bits[i]) {
                    WriteShort(ms, 5000); 
                    WriteShort(ms, 24000);
                    WriteShort(ms, short.MaxValue);
                    WriteShort(ms, 24000);
                    WriteShort(ms, 5000);
                } else {
                    WriteShort(ms, -5000);
                    WriteShort(ms, -24000);
                    WriteShort(ms, short.MinValue);
                    WriteShort(ms, -24000);
                    WriteShort(ms, -5000);
                }
            }
            var buf = ms.GetBuffer();
            Array.Resize(ref buf, bits.Length * 10);
            return buf;
        }

        private static void WriteShort(MemoryStream ms, short value)
        {
            var bytes = BitConverter.GetBytes(value);
            ms.WriteByte(bytes[0]);
            ms.WriteByte(bytes[1]);
        }
    }
}
