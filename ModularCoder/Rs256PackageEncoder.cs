﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrorCorrection;

namespace ModularCoder
{
    class Rs256PackageEncoder
    {
        internal const int MagicPolynomial = 285;

        Rs256Encoder encoder;

        int msgSize;
        int dataSize;
        int paritySize;

        public Rs256PackageEncoder(int parityBytes)
        {
            paritySize = parityBytes;
            msgSize = 255 - paritySize;
            dataSize = msgSize - 1;
            encoder = new Rs256Encoder(256, msgSize, MagicPolynomial);
        }

        public byte[] EncodePackage(byte[] data, int offset, int size)
        {
            if (size == 0 || size > dataSize)
                throw new ArgumentOutOfRangeException(nameof(size));
            if (data.Length - offset < size)
                throw new Exception("Not enough data");

            var blockBuf = new byte[255];

            Buffer.BlockCopy(data, offset, blockBuf, paritySize, size);

            blockBuf[254] = (byte)size;

            encoder.Encode(blockBuf);

            return blockBuf;
        }
    }
}
