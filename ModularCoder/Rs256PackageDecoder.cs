﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrorCorrection;

namespace ModularCoder
{
    class Rs256PackageDecoder
    {
        Rs256Decoder decoder;

        int msgSize;
        int dataSize;
        int paritySize;

        public Rs256PackageDecoder(int parityBytes)
        {
            paritySize = parityBytes;
            msgSize = 255 - paritySize;
            dataSize = msgSize - 1;
            decoder = new Rs256Decoder(256, msgSize, Rs256PackageEncoder.MagicPolynomial);
        }

        public byte[] DecodePackage(byte[] package, int offset)
        {
            if (package.Length - offset < 255)
                throw new Exception("Wrong size package");

            var blockBuf = new byte[decoder.CodeWordSize];

            Buffer.BlockCopy(package, offset, blockBuf, 0, blockBuf.Length);

            decoder.Decode(blockBuf);

            var actualDataSize = blockBuf[254];

            var data = new byte[actualDataSize];

            Buffer.BlockCopy(blockBuf, paritySize, data, 0, data.Length);

            return data;
        }
    }
}
