﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularCoder
{
    class NRZDecoder
    {
        public unsafe byte[] Decode(byte[] pcmData)
        {
            byte[] bytes = new byte[pcmData.Length / 80];
            fixed(void* ptrData = pcmData) {
                short* shortPtrData = (short*)ptrData;
                for (int i = 0; i < (pcmData.Length / 80); i++) {
                    bytes[i] = ReadByte(shortPtrData, i * 40);
                }               
            }
            return bytes;
        }

        public unsafe byte ReadByte(short* pcmData, int sampleOffset)
        {
            int b = 0;
            for (int i = 0; i < 8; i++) {
                b += ReadBit(pcmData, sampleOffset + i * 5) << i;
            }
            return (byte)b;
        }

        public unsafe int ReadBit(short* pcmData, int sampleOffset)
        {
            int mean = (int)(pcmData[sampleOffset] * 0.0f + pcmData[sampleOffset + 1] * 0.15f
                + pcmData[sampleOffset + 2] * 0.7f 
                + pcmData[sampleOffset + 3] * 0.15f + pcmData[sampleOffset + 4] * 0.0f);
            if (mean > 0)
                return 1;
            else
                return 0;
        } 
    }
}
