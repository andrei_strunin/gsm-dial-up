﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrorCorrection;
using NAudio.Wave;
using NAudio.Wave.Compression;

namespace ModularCoder
{
    class Program
    {
        static void Main(string[] args)
        { 
            var fileBytes = File.ReadAllBytes("C:\\Music\\AngelBullet.ogg");
            var rs2 = RSEncode(fileBytes);
            var nrz2 = NrzEncode(rs2);

            File.WriteAllBytes("C:\\Music\\AngelBullet.ogg.rs.nrz", nrz2);
            
            /*var nrz = File.ReadAllBytes("C:\\Music\\AngelBullet.ogg.rs.nrz.raw");
            var rs = NrzDecode(nrz);

            var data = RSDecode(rs);

            File.WriteAllBytes("C:\\Music\\deRS_deNRZ_AngelBullet.ogg", data);*/

            /*var enc = new Encoder1000Hz2();
            var dec = new Decoder1000Hz2();
            /*var encodedData = enc.Encode(File.ReadAllBytes("C:\\Music\\AngelBullet.ogg"));

            File.WriteAllBytes("C:\\Music\\AngleBullet.ogg.enc", encodedData);*/

            /*var decodedData = dec.Decode(File.ReadAllBytes("C:\\Music\\AngleBullet.ogg.enc.raw"));

            File.WriteAllBytes("C:\\Music\\AngelBulletDEC.ogg", decodedData);*/

            //File.WriteAllBytes("enigmaTest", encodedData);

            /*NAudio.Wave.Gsm610WaveFormat f = new Gsm610WaveFormat();
            AcmStream acm = new AcmStream(f, new WaveFormat(8000, 16, 1));

            var b = File.ReadAllBytes("C:\\Music\\gsmN.raw");
            MemoryStream converted = new MemoryStream();
            int srcOffset = 0;
            while (true) {
                int toConvert = Math.Min(acm.SourceBuffer.Length, b.Length - srcOffset);
                Buffer.BlockCopy(b, srcOffset, acm.SourceBuffer, 0, toConvert);
                var convertedSize = acm.Convert(toConvert, out int convertedBytes);

                converted.Write(acm.DestBuffer, 0, convertedSize);

                srcOffset += convertedBytes;

                if (toConvert < acm.SourceBuffer.Length)
                    break;
            }

            File.WriteAllBytes("C:\\Music\\16pcmDecFromGsmN.raw", converted.GetBuffer());*/


            /*Rs256PackageEncoder rs256enc = new Rs256PackageEncoder(14);
            Rs256PackageDecoder rs256dec = new Rs256PackageDecoder(14);

            var r = new Random(23);
            byte[] data = new byte[240];
            for (int i = 0; i < data.Length; i++) {
                data[i] = (byte)r.Next(256);
            }

            var encoded = rs256enc.EncodePackage(data, 0, 210);

            var decoded = rs256dec.DecodePackage(encoded, 0);

            Array.Resize(ref data, 210);

            ErrorCorrection.ArrayHelpers.CheckArrayEquals(data, decoded);*/
        }

        private static byte[] RSEncode(byte[] pcm)
        {
            MemoryStream ms = new MemoryStream();
            Rs256PackageEncoder rs256enc = new Rs256PackageEncoder(30);
            for (int i = 0; i < pcm.Length; i += 224) {
                var rsPack = rs256enc.EncodePackage(pcm, i, Math.Min(224, pcm.Length - i));
                ms.Write(rsPack, 0, 255);
            }

            var buf = ms.GetBuffer();
            Array.Resize(ref buf, (int)ms.Length);
            return buf;
        }

        private static byte[] RSDecode(byte[] rs)
        {
            MemoryStream ms = new MemoryStream();
            Rs256PackageDecoder rs256dec = new Rs256PackageDecoder(30);
            byte[] packageData;
            for (int i = 0; i < rs.Length; i+=255) {
                packageData = rs256dec.DecodePackage(rs, i);
                if (packageData.Length < 240)
                    ;
                ms.Write(packageData, 0, packageData.Length);
            }

            var buf = ms.GetBuffer();
            Array.Resize(ref buf, (int)ms.Length);
            return buf;
        }

        static public byte[] NrzEncode(byte[] data)
        {
            var enc = new NRZEncoder();
            return enc.Encode(data);
        }

        static public byte[] NrzDecode(byte[] nrz)
        {
            var dec = new NRZDecoder();
            return dec.Decode(nrz);
        }
    }
}
