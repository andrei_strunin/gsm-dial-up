﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace Enigma
{
    class RecorderStream : Stream
    {
        public override bool CanRead => true;
        public override bool CanSeek => false;
        public override bool CanWrite => false;
        public override long Length => throw new NotImplementedException();
        public override long Position { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        private IntPtr device;
        private IntPtr memBuf;
        //private byte[] excel = new byte[0];

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            device = Alc.CaptureOpenDevice(AudioCapture.DefaultDevice, 48000, ALFormat.Stereo16, 12000);
            Alc.CaptureStart(device);

            memBuf = Marshal.AllocHGlobal(12000 * 4);
        }

        public void Stop()
        {
            Alc.CaptureStop(device);
            Alc.CaptureCloseDevice(device);

            Marshal.FreeHGlobal(memBuf);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int capturedBytes = 0;
            while (capturedBytes < count) {
                Thread.Sleep(1);
                Alc.GetInteger(device, AlcGetInteger.CaptureSamples, 4, out int captured);
                Alc.CaptureSamples(device, memBuf, captured);
                unsafe
                {
                    fixed (void* ptr = &buffer[capturedBytes])
                        Buffer.MemoryCopy(memBuf.ToPointer(), ptr, buffer.LongLength, Math.Min(captured * 4, buffer.LongLength - capturedBytes));
                }
                capturedBytes += captured * 4;               
            }
            return count;
        }

        /*private unsafe void SaveExcel(IntPtr memBuf, int sentBytes, int capturedBytes)
        {
            excel = new byte[capturedBytes - sentBytes];
            fixed (void* ptr = excel)
                Buffer.MemoryCopy(IntPtr.Add(memBuf, sentBytes).ToPointer(), ptr, excel.LongLength, capturedBytes - sentBytes);
        }*/

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}
