﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using Concentus;
using Concentus.Structs;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Enigma
{
    class Program
    {
        const int SampleRate = 48000;
        const int opusPackageSize = 286;

        static void Main(string[] args)
        {
            Console.WriteLine("0 if listener");
            if (Console.ReadLine() == "0") {
                StartListener();
            } else
                StartStreamer();        
        }

        private static void StartListener()
        {
            using (var context = new AudioContext(AudioContext.DefaultDevice, SampleRate)) {
                int[] buffers = AL.GenBuffers(10);
                int source = AL.GenSource();
                int state;
                int readed;
                int bufN = 0;
                var byteBuffer = new byte[8192];
                Console.WriteLine("Enter password");
                SecureTcpClient client = new SecureTcpClient(Console.ReadLine());
                Console.WriteLine("Enter IP");
                client.Connect(Console.ReadLine());                

                OpusDecoder decoder = new OpusDecoder(48000, 2);

                short[] shortBuffer = new short[5760];
                AL.Source(source, ALSourcef.Gain, 0.3f);
                while (true) {
                    readed = client.Read(byteBuffer, 0, opusPackageSize);
                    var decoded = decoder.Decode(byteBuffer, 0, opusPackageSize, shortBuffer, 0, 1920);
                    AL.BufferData(buffers[bufN], ALFormat.Stereo16, shortBuffer, decoded * 4, SampleRate);
                    AL.SourceQueueBuffer(source, buffers[bufN]);
                    if (AL.GetSourceState(source) == ALSourceState.Stopped) {
                        AL.GetSource(source, ALGetSourcei.BuffersQueued, out int queued);
                        //Console.WriteLine(queued);
                        if (queued > buffers.Length - 2) {
                            AL.SourcePlay(source);
                            Console.WriteLine("PLAY");
                        }
                    }
                    if (AL.GetSourceState(source) == ALSourceState.Initial)
                        AL.SourcePlay(source);
                    bufN++;
                    bufN %= buffers.Length;
                    int processed = 0;
                    while (AL.GetSourceState(source) == ALSourceState.Playing && processed == 0) {
                        Thread.Sleep(0);
                        AL.GetSource(source, ALGetSourcei.BuffersProcessed, out processed);
                        Console.WriteLine(processed);
                    }
                    if (processed > 0)
                        AL.SourceUnqueueBuffers(source, processed);
                    if (readed < opusPackageSize)
                        break;
                }

                do {
                    Thread.Sleep(250);
                    AL.GetSource(source, ALGetSourcei.SourceState, out state);
                }
                while ((ALSourceState)state == ALSourceState.Playing);

                AL.SourceStop(source);
                AL.DeleteSource(source);
                AL.DeleteBuffers(buffers);
            }
        }

        private static void StartStreamer()
        {
            Console.WriteLine("Enter password");           
            SecureTcpListener listener = new SecureTcpListener(Console.ReadLine());
            listener.Start();

            var client = listener.AcceptTcpClient();
            var audioStream = new RecorderStream();// new FileStream("audio.raw", FileMode.Open);
            byte[] buffer = new byte[1920 * 2 * 2];
            int readed;
            OpusEncoder encoder = new OpusEncoder(48000, 2, Concentus.Enums.OpusApplication.OPUS_APPLICATION_VOIP) {
                Bitrate = 56 * 1024,
                UseVBR = false,
            };
            audioStream.Start();
            while (true) {
                readed = audioStream.Read(buffer, 0, buffer.Length);
                short[] shortBuffer = new short[buffer.Length / 2];
                unsafe
                {
                    fixed (void* ptr = shortBuffer)
                        Marshal.Copy(buffer, 0, new IntPtr(ptr), buffer.Length);
                }
                Array.Clear(buffer, 0, buffer.Length);
                var packageSize = encoder.Encode(shortBuffer, 0, 1920, buffer, 0, buffer.Length);
                Console.WriteLine(packageSize);
                client.Write(buffer, 0, packageSize);
                Thread.Sleep(5);
                if (readed < buffer.Length)
                    break;
            }
        }
    }
}
