﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Enigma
{
    class SecureTcpListener
    {
        TcpListener listener;
        string pass;

        public SecureTcpListener(string pass)
        {
            this.pass = pass;
            listener = new TcpListener(IPAddress.Any, 10802);
        }

        public void Start()
        {
            listener.Start();
        }

        public SecureTcpClient AcceptTcpClient()
        {
            return new SecureTcpClient(listener.AcceptTcpClient(), pass);
        }
    }
}
