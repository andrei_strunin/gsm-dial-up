﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Enigma
{
    class SecureTcpClient
    {
        TcpClient client;
        NetworkStream stream;
        CryptoStream cryptoStream;

        Aes aes;
        ICryptoTransform transform;

        public SecureTcpClient(string pass)
            : this(new TcpClient(), pass)
        {
        }

        internal SecureTcpClient(TcpClient client, string pass)
        {
            aes = new AesManaged();

            aes.Key = GetKey(pass);
            //aes.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            this.client = client;
            if (client.Connected) {
                stream = client.GetStream();
                var iv = new byte[aes.IV.Length];
                stream.Read(iv, 0, iv.Length);
                aes.IV = iv;
                transform = aes.CreateEncryptor();

                cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Write);
            }
        }

        public void Connect(string address)
        {
            client.Connect(address, 10802);
            stream = client.GetStream();
            stream.Write(aes.IV, 0, aes.IV.Length);
            transform = aes.CreateDecryptor();

            cryptoStream = new CryptoStream(stream, transform, CryptoStreamMode.Read);
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            cryptoStream.Write(buffer, offset, count);
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            return cryptoStream.Read(buffer, offset, count);
        }

        public byte[] GetKey(string password)
        {
            SHA256Managed sha = new SHA256Managed();          
            return sha.ComputeHash(Encoding.Unicode.GetBytes(password));
        }
    }
}
